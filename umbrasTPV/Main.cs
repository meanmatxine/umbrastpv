﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV;
using umbrasTPV.Clases;
using umbrasTPV.Formularios;
 
namespace umbrasTPV
{

    public partial class Main : Form
    {
        //datas
        DataView productos;
        DataView productosTipo;
        //DataView sesionView;
        //botones
        List<Button> botones;//botones producto
        List<Button> botonesTipo;//botones tipo
        //tablas
        TBProductType TBproductostipo;
        TBProducts tbProducts;
        TBSesion tbSesion;
        TBPedidos tbPedidos;
        //TBPedidosDetalle tbDetalles;
        //listas
        List<Producto> ventaProductos;
        //BindingList<Producto> bindingProductos;
        decimal totalPedido;
        //Pedido
        public Pedido pedidoTemp;
        //public List<PedidoDetalle> pedidoDet;
        //sesion activa
        Sesion activeSesion;
        //Grid pedidos
        DataTable gridPedidos;


        public Main()
        {
            InitializeComponent();
          
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //new SQLite();
            this.Show();
            //sesion
            tbSesion = new TBSesion();
            activeSesion = tbSesion.SELECT_ACTIVE();
            if (activeSesion == null)
            {
                Console.WriteLine("sin sesion");
                //No hay sesion
                IniciarSesion ise = new IniciarSesion();
                ise.ShowDialog();
                activeSesion = tbSesion.SELECT_ACTIVE();
                start();
                //this.Close();
            }
            else {
                Console.WriteLine("con sesion");
                start();
            }
        }

        private void start() {
            //PRUEBAS
            tbPedidos = new TBPedidos();
            Pedido p= tbPedidos.GetLast();
            //Console.WriteLine(p.pedidoID + "-" + p.pedidoTotal + "-"+p.pedidoEntregado + "-" + p.pedidoDevuelto);
            //posicion de botones
            Point newLoc = new Point(10, 10);
            //tabla
            //tableVentas.RowCount = 1;
            /*tableVentas.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            tableVentas.Controls.Add(new Label() { Text = "Producto" }, 0, 0);
            tableVentas.Controls.Add(new Label() { Text = "Cant" }, 1, 0);
            tableVentas.Controls.Add(new Label() { Text = "Precio ud" }, 2, 0);
            tableVentas.Controls.Add(new Label() { Text = "Total" }, 3, 0);
            tableVentas.Controls.Add(new Label() { Text = "Quitar" }, 4, 0);*/
            //listaVenta.Rows.Insert(0, "Producto", "Cantidad", "Precio Ud", "Total", "Quitar");
            //botones tipo
            botonesTipo = new List<Button>();
            //cogemos tipos
            TBproductostipo = new TBProductType();
            productosTipo = TBproductostipo.SELECT_ALL();
            //creamos los botones en el formulario
            newLoc = new Point(10, 10);
            for (int i = 0; i < productosTipo.Table.Rows.Count; i++)
            {
                ProductoTipo pt = new ProductoTipo(productosTipo.Table.Rows[i]);
                Button b = new Button();
                b.Text = pt.tipoName;
                b.Name = pt.tipoID.ToString();
                b.Size = new Size(100, 30);
                b.Location = newLoc;
                b.Click += new EventHandler(changeType);
                newLoc.Offset(b.Width + 10, 0);
                panelTipos.Controls.Add(b);
            }
            //BotonesProducto
            botones = new List<Button>();
            //productos de la venta
            ventaProductos = new List<Producto>();
            //cogemos productos al inicializar
            tbProducts = new TBProducts();
            //inicializar productos
            inicializarProductos();
            productos = tbProducts.SELECT_TYPE(1);
            //creamos los botones en el formulario
            cambiarBotones();
            newLoc = new Point(10, 10);
            precioTotal.Text = "0 €";
            totalPedido = 0;
            //iniciamos pedido
            pedidoTemp = new Pedido(activeSesion.sesionID);
            //pedidoDet = new List<PedidoDetalle>();
            //grid
            generarGrid();
            datosPedidoGrid.DataSource = gridPedidos;
            datosPedidoGrid.Columns[1].Width = 130;
            datosPedidoGrid.Columns[2].Width = 60;
            datosPedidoGrid.Columns[3].Width = 60;
            datosPedidoGrid.Columns[4].Width = 60;
            //dataGridView1.AutoGenerateColumns = true;
            //focus en CB
            CBTextBox.Focus();
            CBTextBox.TabIndex = 1;
            dineroEntregado.TabIndex = 2;
        }

        private void dbcTest_Click(object sender, EventArgs e)
        {

        }
        //inicializar pedidos
        void inicializarProductos() {
            productos = tbProducts.SELECT_ALL();
            for (int i = 0; i < productos.Table.Rows.Count; i++) {
                Producto pTemp= new Producto(productos.Table.Rows[i]);
                pTemp.productoCantidad = 0;
                ventaProductos.Add(pTemp);
            }
        }
        //boton tipo
        private void changeType(object sender, EventArgs e) {
            Button b = sender as Button;
            productos = tbProducts.SELECT_TYPE(int.Parse(b.Name));
            cambiarBotones();
        }

        //cambiar botones segun tipo
        private void cambiarBotones() {
            Point newLoc = new Point(10, 10);
            productosPanel.Controls.Clear();
            for (int i = 0; i < productos.Table.Rows.Count; i++)
            {
                Producto p = new Producto(productos.Table.Rows[i]);
                p.productoCantidad = 0;
                //ventaProductos.Add(p);
                Button b = new Button();
                b.Text = p.productoName;
                b.Name = p.productoID.ToString();
                b.Size = new Size(100, 100);
                b.Location = newLoc;
                b.Click += new EventHandler(addItem);
                newLoc.Offset(b.Width + 10, 0);
                if (newLoc.X > 550)
                {
                    newLoc.X = 10;
                    newLoc.Y += b.Height + 10;
                }
                botones.Add(b);
                productosPanel.Controls.Add(b);
            }
        }

        //evento click de los botones, añade items
        private void addItem(object sender, EventArgs e) {
            Button b = sender as Button;
            //Console.WriteLine("entra por boton");
            foreach (Producto pr in ventaProductos)
            {
                //Console.WriteLine("aqui");
                if (pr.productoID.ToString() == b.Name)
                {
                    AddItemToList(pr);
                    break;
                }
            }
        }

        //Entrada de item 
        void AddItemToList(Producto p) {
            //MessageBox.Show(p.productoName + "-" + p.productoCantidad);
            p.productoCantidad++;
            //MessageBox.Show(p.productoName + "-"+p.productoCantidad);
            actualizarLista();
        }

        //actualizar lista
        private void actualizarLista() {
            //limpiarTabla();
            totalPedido = 0;
            //DATA GRID
            generarGrid();
            //lista pedido
            foreach (Producto p in ventaProductos) {
                if (p.productoCantidad > 0)
                {
                    //list view
                    //int linea = tableVentas.RowCount;
                    //Console.WriteLine(tableVentas.RowCount);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoName }, 0, linea);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoCantidad.ToString() }, 1, linea);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoPrecio.ToString() }, 2, linea);
                    //tableVentas.Controls.Add(new Label() { Text = (p.productoPrecio * p.productoCantidad).ToString() }, 3, linea);
                    Button b = new Button();
                    b.Text = "X";
                    b.Name = p.productoID.ToString();
                    b.Click += new EventHandler(removeItem);
                    //tableVentas.Controls.Add(b, 4, linea);
                    //tableVentas.RowCount++;
                    totalPedido += (decimal)(p.productoPrecio * p.productoCantidad);
                    //pedido det
                    PedidoDetalle pd = new PedidoDetalle();
                    pd.productoID = p.productoID;
                    pd.pdCantidad = p.productoCantidad;
                    //pedidoDet.Add(pd);
                    //DATAGRID
                    decimal total = p.productoPrecio * p.productoCantidad;
                    gridPedidos.Rows.Add(new Object[] { p.productoName, p.productoCantidad, p.productoPrecio, total });
                    //bindingProductos.Add(p);
                }
                //cambiar total
                precioTotal.Text = totalPedido.ToString()+" €";
                //DATAGRID
                datosPedidoGrid.DataSource = gridPedidos;
                CBTextBox.Focus();
            }
        }

        //generarGrid
        void generarGrid() {
            gridPedidos = new DataTable();
            /*DataColumn colProducto;
            colProducto=new DataColumn("Producto")
            colProducto.*/
            gridPedidos.Columns.Add(new DataColumn("Producto"));
            gridPedidos.Columns.Add(new DataColumn("Cantidad"));
            gridPedidos.Columns.Add(new DataColumn("Precio"));
            gridPedidos.Columns.Add(new DataColumn("Total"));
        }

        //limpiar la tabla
        /*private void limpiarTabla() {
            //Limpiar list view
            tableVentas.Controls.Clear();
            tableVentas.RowStyles.Clear();
            tableVentas.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            tableVentas.RowCount = 1;
            tableVentas.Controls.Add(new Label() { Text = "Producto" }, 0, 0);
            tableVentas.Controls.Add(new Label() { Text = "Cant" }, 1, 0);
            tableVentas.Controls.Add(new Label() { Text = "Precio ud" }, 2, 0);
            tableVentas.Controls.Add(new Label() { Text = "Total" }, 3, 0);
            tableVentas.Controls.Add(new Label() { Text = "Quitar" }, 4, 0);
            //limpiar pedido det
            pedidoDet.Clear();
        }*/

        //click de la lista (quita items)
        private void removeItem(object sender, EventArgs e)
        {
            Button b = sender as Button;
            foreach (Producto pr in ventaProductos)
            {
                Console.WriteLine("aqui");
                if (pr.productoID.ToString() == b.Name)
                {
                    pr.productoCantidad--;
                    if (pr.productoCantidad < 0) { pr.productoCantidad = 0; }
                    actualizarLista();
                    break;
                }
            }
        }

        //quitar objeto en Grid
        private void datosPedidoGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) {
                //quitarlo del grid
                foreach (Producto pr in ventaProductos) {
                    if (pr.productoName == datosPedidoGrid.Rows[e.RowIndex].Cells[1].Value.ToString()) {
                        pr.productoCantidad--;
                        if (pr.productoCantidad < 0) { pr.productoCantidad = 0; }
                        actualizarLista();
                        break;
                    }
                }
                //quitarlo del detalle
               
            }
        }

        //cobrar
        private void pedidoFinalizar_Click(object sender, EventArgs e)
        {
            if (dineroEntregado.Text != null && dineroEntregado.Text != "") {
                if (decimal.Parse(dineroEntregado.Text) < totalPedido)
                {
                    MessageBox.Show("Ha pagado menos de lo que cuesta el pedido", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (totalPedido <= 0)
                {
                    MessageBox.Show("No hay productos en el pedido", "No hay pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    //Datos pedido
                    DateTime localDate = DateTime.Now;
                    var culture = new CultureInfo("es-ES");
                    pedidoTemp.pedidoFecha = localDate.ToString(culture);
                    pedidoTemp.pedidoTotal = (decimal)totalPedido;
                    pedidoTemp.pedidoEntregado = decimal.Parse(dineroEntregado.Text);
                    //a devolver 
                    Decimal dev= (decimal)(decimal.Parse(dineroEntregado.Text) - totalPedido);
                    dev = Math.Round(dev, 2);
                    pedidoTemp.pedidoDevuelto = dev;
                    //TBPedidos.INSERT(p);
                    //Mostrar formulario venta Final
                    pedidoFinal pf = new pedidoFinal(pedidoTemp, ventaProductos);
                    pf.ShowDialog();
                    //tras cerrar limpia el pedido y genera uno nuevo si el pedido no ha sido cancelado
                    if (!pf.pedidoCancelado)
                    {
                        nuevoPedido();
                    }
                }
            }
        }

        private void nuevoPedido() {
            pedidoTemp = null;
            //pedidoDet = null;
            ventaProductos = null;
            ventaProductos = new List<Producto>();
            pedidoTemp = new Pedido(activeSesion.sesionID);
            //pedidoDet = new List<PedidoDetalle>();
            dineroEntregado.Text = "0";
            totalPedido = 0;
            precioTotal.Text = totalPedido.ToString() + " €";
            this.Hide();
            Main m = new Main();
            m.Show();
        }

        //codigo barras
        private void CBTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) {
                //buscar por codigo de barras
                Producto pTemp = tbProducts.SELECT_BY_CB(CBTextBox.Text);
                //agregarlo a productos
                //Console.WriteLine(pTemp.productoID);
                if (pTemp != null)
                {
                    Console.WriteLine("lo añade");
                    //buscar el producto en la lista y lo añade
                    foreach (Producto p in ventaProductos) {
                        if (p.productoID == pTemp.productoID) {
                            AddItemToList(p);
                            break;
                        }
                    }
                }
                else
                {
                    //mensaje Error
                }
                //limpiar texto
                CBTextBox.Text = "";
            }
        }
        
        //cobrar con intro
        private void dineroEntregado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                if (dineroEntregado.Value > 100)
                {
                    /*Console.WriteLine("Codigo de barras");
                    //buscar por codigo de barras
                    Producto pTemp = tbProducts.SELECT_BY_CB(dineroEntregado.Value.ToString());
                    //agregarlo a productos
                    //Console.WriteLine(pTemp.productoID);
                    if (pTemp != null)
                    {
                        Console.WriteLine("lo añade");
                        //buscar el producto en la lista y lo añade
                        foreach (Producto p in ventaProductos)
                        {
                            if (p.productoID == pTemp.productoID)
                            {
                                AddItemToList(p);
                                break;
                            }
                        }
                    }
                    //limpiar texto
                    CBTextBox.Text = "";*/
                }
                else {
                    Console.WriteLine("Pagar");
                    pedidoFinalizar.PerformClick();
                }  
            }
        }

        //nuevo pedido
        private void newPedido_Click(object sender, EventArgs e)
        {
            nuevoPedido();
        }

        //menu nuevo pedido
        private void nuevoPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            nuevoPedido();
        }
        //boton modificar productos
        private void modificarProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Productos p = new Productos();
            p.ShowDialog();
        }
        //boton informe sesion
        private void informeDeSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            informeSesion();
        }

        //cerrar la sesion
        void cerrarSesion() {

        }

        //informe de sesion
        void informeSesion() {
            /*int alturaPapel;
            */

            PrintDocument pd = new PrintDocument();
            
            
            //impresion
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.Print();
        }

        //informe de sesion
        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            //papel A4 Vertical
            e.PageSettings.PaperSize = new PaperSize("A4", 850, 1100);
            //total sesion
            //float sesionTotal;
            int lineaInicial = 50;
            int offsetY = 15;
            int separacion = 15;
            float fontHeight = 10;
            //pedidos sesion
            //inicializar pedidos
            tbPedidos = new TBPedidos();
            DataView pedidosSesion = tbPedidos.pedidosDeSesion(activeSesion.sesionID);
            List<Pedido> pedidosSList = new List<Pedido>();
            decimal sumaPedidos = 0;
            //descuadre
            Graphics g = e.Graphics;
            Font fBody = new Font("LucidaConsole", fontHeight, FontStyle.Regular);
            Font fTitle = new Font("LucidaConsole", fontHeight, FontStyle.Bold);
            Font mainTitle = new Font("LucidaConsole", 15, FontStyle.Bold);
            SolidBrush sb = new SolidBrush(Color.Black);
            //titulo
            g.DrawString("Informe de Sesion de TPV", mainTitle, sb, 10, 10);
            //hora inicio
            g.DrawString("Hora de Inicio de Sesión: ", fTitle, sb, 10, lineaInicial);
            g.DrawString(activeSesion.sesionStart, fBody, sb, 200, lineaInicial);
            //offsetY += (int)fontHeight;
            //Dinero Inicial
            g.DrawString("Dinero inicial de la Sesión: ", fTitle, sb, 10, lineaInicial + offsetY);
            g.DrawString(activeSesion.sesionDineroInicial.ToString(), fBody, sb, 200, lineaInicial + offsetY);
            offsetY += separacion;
            //dinero Final
            g.DrawString("Dinero final de la Sesión: ", fTitle, sb, 10, lineaInicial + offsetY);
            g.DrawString(activeSesion.sesionDineroFinal.ToString(), fBody, sb, 200, lineaInicial + offsetY);
            offsetY += (separacion);
            int cajaFinalPos = offsetY;
            offsetY += (separacion*2);
            //Tabla header
            RectangleF[] rects=new RectangleF[4];
            rects[0] = new RectangleF(10, lineaInicial + offsetY, 100, 30);
            rects[1] = new RectangleF(110, lineaInicial + offsetY, 80, 30);
            rects[2] = new RectangleF(190, lineaInicial + offsetY, 120, 30);
            rects[3] = new RectangleF(310, lineaInicial + offsetY, 120, 30);
            g.DrawRectangles(Pens.Black, rects);
            g.DrawString("Pedido", mainTitle, sb, 20, lineaInicial + offsetY+1);
            g.DrawString("Total", mainTitle, sb, 120, lineaInicial + offsetY+1);
            g.DrawString("Entregado", mainTitle, sb, 200, lineaInicial + offsetY+1);
            g.DrawString("Devuelto", mainTitle, sb, 320, lineaInicial + offsetY+1);
            offsetY += (separacion*2);
            //tabla pedido
            for (int i = 0; i < pedidosSesion.Count; i++) {
                //tabla
                rects[0] = new RectangleF(10, lineaInicial + offsetY, 100, 30);
                rects[1] = new RectangleF(110, lineaInicial + offsetY, 80, 30);
                rects[2] = new RectangleF(190, lineaInicial + offsetY, 120, 30);
                rects[3] = new RectangleF(310, lineaInicial + offsetY, 120, 30);
                g.DrawRectangles(Pens.Black, rects);
                //texto
                Pedido p = new Pedido(pedidosSesion.Table.Rows[i]);
                g.DrawString(p.pedidoID.ToString(), mainTitle, sb, 20, lineaInicial + offsetY+2);
                g.DrawString(p.pedidoTotal.ToString()+" €", mainTitle, sb, 120, lineaInicial + offsetY+2);
                //Console.WriteLine(p.pedidoEntregado);
                g.DrawString(p.pedidoEntregado.ToString() + " €", mainTitle, sb, 200, lineaInicial + offsetY+2);
                g.DrawString(p.pedidoDevuelto.ToString() + " €", mainTitle, sb, 320, lineaInicial + offsetY+2);
                //paso linea
                offsetY += (separacion * 2);
                //compruebo fin de hoja
                sumaPedidos += p.pedidoTotal;
                Console.WriteLine(e.PageSettings.PrintableArea.Height);
                if (offsetY >= e.PageSettings.PrintableArea.Height - 10) {
                    e.HasMorePages = true;
                    offsetY = 15;
                    //titulo
                    g.DrawString("Informe de Sesion de TPV", mainTitle, sb, 10, 10);
                    //Tabla header
                    rects[0] = new RectangleF(10, lineaInicial + offsetY, 100, 30);
                    rects[1] = new RectangleF(110, lineaInicial + offsetY, 80, 30);
                    rects[2] = new RectangleF(190, lineaInicial + offsetY, 120, 30);
                    rects[3] = new RectangleF(310, lineaInicial + offsetY, 100, 30);
                    g.DrawRectangles(Pens.Black, rects);
                    g.DrawString("Pedido", mainTitle, sb, 20, lineaInicial + offsetY);
                    g.DrawString("Total", mainTitle, sb, 120, lineaInicial + offsetY);
                    g.DrawString("Entregado", mainTitle, sb, 200, lineaInicial + offsetY);
                    g.DrawString("Devuelto", mainTitle, sb, 320, lineaInicial + offsetY);
                    offsetY += offsetY;
                }
                else
                {
                    e.HasMorePages = false;
                }

                

            }
            //caja
            g.DrawString("Caja Teórica Actual: ", fTitle, sb, 10, lineaInicial + offsetY);
            g.DrawString((activeSesion.sesionDineroInicial + sumaPedidos).ToString(), fBody, sb, 200, cajaFinalPos);

        }
        //puntos x comas en el precio
        private void dineroEntregado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(',')) {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        //boton fin de sesion
        private void finSesionB_Click(object sender, EventArgs e)
        {
            //sacar mensaje
            DialogResult result=MessageBox.Show("¿Desea finalizar la Sesión?, Podra Cuadrar la caja y reajustar el dinero disponible", "Cerrar Sesión", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                //Console.WriteLine("Reset Sesion!!");
                CerrarSesion cs = new CerrarSesion();
                cs.ShowDialog();
            }
            else {
                //nada
            }
        }

        private void cobrarDirecto_Click(object sender, EventArgs e)
        {
            if (dineroEntregado.Text != null && dineroEntregado.Text != "")
            {
                if (decimal.Parse(dineroEntregado.Text) < totalPedido)
                {
                    MessageBox.Show("Ha pagado menos de lo que cuesta el pedido", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (totalPedido <= 0)
                {
                    MessageBox.Show("No hay productos en el pedido", "No hay pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //Datos pedido
                    DateTime localDate = DateTime.Now;
                    var culture = new CultureInfo("es-ES");
                    pedidoTemp.pedidoFecha = localDate.ToString(culture);
                    pedidoTemp.pedidoTotal = (decimal)totalPedido;
                    pedidoTemp.pedidoEntregado = decimal.Parse(dineroEntregado.Text);
                    //a devolver 
                    Decimal dev = (decimal)(decimal.Parse(dineroEntregado.Text) - totalPedido);
                    dev = Math.Round(dev, 2);
                    pedidoTemp.pedidoDevuelto = dev;
                    //insertamos el pedido
                    tbPedidos.INSERT(pedidoTemp);
                    //cogemos el ultimo pedido
                    Pedido ultimo = tbPedidos.GetLast();
                    //agregamos el id al detalle e insertamos en DBC
                    /*tbDetalles = new TBPedidosDetalle();
                    foreach (PedidoDetalle pde in pedidoDet)
                    {
                        pde.pedidoID = ultimo.pedidoID;
                        tbDetalles.Insert(pde);
                    }*/
                    //nuevo Pedido
                    nuevoPedido();
                }

            }
            
        }

        //formulario ver pedidos
        private void verPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VerPedidos vp = new VerPedidos();
            vp.ShowDialog();
        }

        private void dineroEntregado_Enter(object sender, EventArgs e)
        {
            dineroEntregado.Select(0, 4);
        }

        private void dineroEntregado_Click(object sender, EventArgs e)
        {
            //dineroEntregado.Select(0, 4);
        }
    }
}
