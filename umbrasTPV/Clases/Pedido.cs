﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace umbrasTPV.Clases
{
    //PEDIDO
    public class Pedido
    {
        public int pedidoID;
        public int sesionID;
        public string pedidoFecha;
        public decimal pedidoTotal;
        public decimal pedidoEntregado;
        public decimal pedidoDevuelto;

        public Pedido(DataRow r) {
            pedidoID= (int)r.Field<Int64>("pedidoID");
            sesionID= (int)r.Field<Int64>("sesionID");
            pedidoFecha= r.Field<String>("pedidoFecha");
            pedidoTotal= (decimal)r.Field<double>("pedidoTotal");
            pedidoEntregado = (decimal)r.Field<double>("pedidoEntregado");
            pedidoDevuelto = (decimal)r.Field<double>("pedidoDevuelto");
        }

        public Pedido(int sID) {
            sesionID = sID;          
            pedidoTotal = 0;
            pedidoEntregado = 0;
            pedidoDevuelto = 0;
        }
    }

    //pedidoDetalle
    public class PedidoDetalle {
        public int pedidoID;
        public int productoID;
        public int pdCantidad;

        public PedidoDetalle(DataRow r) {
            pedidoID= (int)r.Field<Int64>("pedidoID");
            productoID=(int)r.Field<Int64>("productoID");
            pdCantidad= (int)r.Field<Int64>("pdCantidad");
        }

        public PedidoDetalle() {

        }
    }

    //tabla pedido
    class TBPedidos {
        private SQLite dbc;
        private DataSet data;
        DataView tabla;

        public TBPedidos()
        {
            dbc = new SQLite();
            dbc.table = "Pedidos";
        }

        //pedidosDeSesion
        public DataView pedidosDeSesion(int sID) {
            dbc.query = "SELECT * FROM Pedidos WHERE sesionID=" + sID;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
        }

        //coger uno
        public DataView SELECT_ONE(int pID) {
            dbc.query = "SELECT * FROM Pedidos WHERE pedidoID=" + pID;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
        }
        //coger uno a Pedido
        public Pedido SELECT_ONE_PEDIDO(int id) {
            dbc.query = "SELECT * FROM Pedidos WHERE pedidoID=" + id;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            //Console.WriteLine(tabla.Table.Rows.Count);
            Pedido p = new Pedido(tabla.Table.Rows[0]);
            return p;
        }

        //insertar pedido
        public void INSERT(Pedido p) {
            string pt, pe, pd;
            pt = p.pedidoTotal.ToString().Replace(",",".");
            pe = p.pedidoEntregado.ToString().Replace(",", ".");
            pd = p.pedidoDevuelto.ToString().Replace(",", ".");
            Console.WriteLine("insertar");
            dbc.query = "INSERT INTO Pedidos (pedidoID, sesionID, pedidoFecha, pedidoTotal, pedidoEntregado, pedidoDevuelto) VALUES (";
            dbc.query += "null, "+p.sesionID+", '"+p.pedidoFecha+"', '"+pt+"', '"+pe+"', '"+pd+"');";
            //Console.WriteLine(dbc.query);
            dbc.executeQuery();
        }

        public Pedido GetLast() {
            dbc.query ="SELECT * FROM Pedidos ORDER BY pedidoID DESC LIMIT 1";
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            Pedido p = null;
            Console.WriteLine(tabla.Table.Rows.Count);
            if (tabla.Table.Rows.Count > 0)
            {
                
                //Console.WriteLine(tabla.Table.Rows.Count);
                 p= new Pedido(tabla.Table.Rows[0]);
            }
            return p;
        }

        //todos a grid
        public DataTable SELECT_ALL_GRID_REVERSE() {
            dbc.query = "SELECT * FROM Pedidos ORDER BY pedidoID DESC";
            SQLiteDataAdapter sqlda = dbc.resultsAdapter();
            DataTable dt = new DataTable();
            sqlda.Fill(dt);
            return dt;
        }

    }

    //tabla pedidos detallados
    class TBPedidosDetalle {
        private SQLite dbc;
        private DataSet data;
        DataView tabla;

        public TBPedidosDetalle()
        {
            dbc = new SQLite();
            dbc.table = "PedidosDetalle";
        }

        //coger detalle de un pedido
        public DataView SELECT_PEDIDO(int pID) {
            dbc.query = "SELECT * FROM PedidosDetalle WHERE pedidoID=" + pID;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
        }

        //detalle a List
        public List<PedidoDetalle> SELECT_ONE_GRID(int id) {
            dbc.query = "SELECT * FROM PedidosDetalle WHERE pedidoID=" + id;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            List<PedidoDetalle> lpd = new List<PedidoDetalle>();
            for (int i = 0; i < tabla.Count; i++) {
                PedidoDetalle pd = new PedidoDetalle(tabla.Table.Rows[i]);
                lpd.Add(pd);
            }
            return lpd;
        }

        public void Insert(PedidoDetalle pd) {
            dbc.query = "INSERT INTO PedidosDetalle (pedidoID, productoID, pdCantidad) VALUES (";
            dbc.query += pd.pedidoID+", "+pd.productoID+", "+pd.pdCantidad+");";
            //Console.WriteLine(dbc.query);
            dbc.executeQuery();
        }
    }
}
