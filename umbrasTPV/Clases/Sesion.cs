﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace umbrasTPV.Clases
{
    class Sesion
    {
        public int sesionID;
        public string sesionStart;//"DD/MM/YYYY HH:MM:SS"
        public string sesionEnd;//"DD/MM/YYYY HH:MM:SS"
        public decimal sesionDineroInicial;
        public decimal sesionDineroFinal;
        public int sesionActiva;//0-1
        public decimal sesionDescuadre;

        public Sesion(DataRow r)
        {
            //Console.WriteLine("new Sesion");
            sesionID = (int)r.Field<Int64>("sesionID");
            sesionStart = r.Field<String>("sesionStart");
            sesionEnd = r.Field<String>("sesionEnd");
            sesionDineroInicial = (decimal)r.Field<double>("sesionDineroInicial");
            sesionDineroFinal = (decimal)r.Field<double>("sesionDineroFinal");
            sesionActiva = (int)r.Field<Int64>("sesionActiva");
            sesionDescuadre = (decimal)r.Field<double>("sesionDescuadre");
            //Console.WriteLine(productoID+","+productoName+","+productoPrecio+","+productoIva+","+productoCantidad);
        }

        public Sesion() {

        }
    }

    class TBSesion
    {
        private SQLite dbc;
        private DataSet data;
        DataView tabla;

        public TBSesion()
        {
            dbc = new SQLite();
            dbc.table = "Sesiones";
        }

        //Seleccionar todos
        public DataView SELECT_ALL()
        {
            dbc.query = "SELECT * FROM Sesiones";
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
        }

        //buscar sesion activa
        public Sesion SELECT_ACTIVE() {
            dbc.query = "SELECT * FROM Sesiones WHERE sesionActiva=1";
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            if (tabla.Table.Rows.Count <= 0 )
            {
                return null;
            }
            else {
                Sesion s = new Sesion(tabla.Table.Rows[0]);
                return s;
            }
        }

        //insertar sesion
        public void INSERT(Sesion s) {
            string sdi, sdf, sdes;
            sdi = s.sesionDineroInicial.ToString().Replace(",", ".");
            sdf=s.sesionDineroFinal.ToString().Replace(",", ".");
            sdes=s.sesionDescuadre.ToString().Replace(",", ".");
            dbc.query = "INSERT INTO Sesiones (sesionID,sesionStart,sesionEnd,sesionDineroInicial,sesionDineroFinal,sesionActiva,sesionDescuadre) VALUES ";
            dbc.query += "(null,'"+s.sesionStart+ "','" + s.sesionEnd + "','" + sdi + "','" + sdf + "'," + s.sesionActiva + ",'"+sdes+"');";
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }

        //updatear el total
        public void UPDATETOTAL(decimal f, int id) {
            dbc.query = "UPDATE Sesiones SET sesionDineroFinal='" + f + "' WHERE sesionID= " + id + ";";
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }

        //Actualizar Sesion
        public void UPDATE(Sesion s) {
            string sdf, sdes;
            sdf = s.sesionDineroFinal.ToString().Replace(",", ".");
            sdes = s.sesionDescuadre.ToString().Replace(",", ".");
            dbc.query = "UPDATE SESIONES SET sesionEnd='"+s.sesionEnd+"', sesionDineroFinal='"+sdf+"', sesionActiva='"+s.sesionActiva+"', sesionDescuadre='"+sdes+"' WHERE sesionID="+s.sesionID+";";
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }

    }
}
