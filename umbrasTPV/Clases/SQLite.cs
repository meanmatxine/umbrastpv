﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Diagnostics;
using System.Data;

namespace umbrasTPV.Clases
{
    class SQLite
    {
        private string sqlConString;
        private SQLiteConnection con;
        private SQLiteCommand command;
        public string table;
        public string query;
        private bool red = false;
        //constructor
        public SQLite()
        {
            //red = true;
            if (red)
            {
                sqlConString = "X:\\umbrasTPV.db";
            }
            else {
                sqlConString = "C:\\temp\\umbrasTPV.db";
            }
            //sqlConString = "C:\\Users\\" + Environment.UserName + "\\Desktop\\umbrasTPV.db";
            if (!System.IO.File.Exists(sqlConString))
            {
                //Console.WriteLine("C:\\Users\\"+Environment.UserName+"\\Desktop\\umbrasTPV.db");
                Console.WriteLine("archivo no existe");
                SQLiteConnection.CreateFile(sqlConString);
                con = new SQLiteConnection("Data Source=" + sqlConString);
                createDBC();
            }
            else
            {
                //Console.Write("archivo existe");
                con = new SQLiteConnection("Data Source=" + sqlConString);
                updateDBC();
            }
        }

        //crear DBC
        private void createDBC()
        {
            createSQLString();
            executeQuery();
            
        }

        //actualizar DBC
        private void updateDBC()
        {
            

        }
        //SQL de base de datos
        private void createSQLString() {
            //tabla Productos
            query = "CREATE TABLE `Productos` (" +
            "`productoID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            "`productoCB` TEXT," +
            "`productoName` TEXT," +
            "`productoTipo` INTEGER," +
            "`productoPrecioSinIva` REAL," +
            "`productoIVA` INTEGER," +
            "`productoPrecio`	REAL," +
            "`productoCantidad` INTEGER" +
            "); ";
            //insertar productos
            query +=
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Pepsi','8410732050001', 1, 0.727, 10, 0.80, 100); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Pepsi 0.0','8410732050002', 1, 0.727, 10, 0.80, 100); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Kas Naranja','8410732050003', 1, 0.727, 10, 0.80, 100); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Kas Limon','8410732050004', 1, 0.727, 10, 0.80, 100); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Donettes Choco','8410732050005', 2, 0.909 , 10, 1, 50); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Camiseta XL Hombre','8410732050011', 3, 8.264, 21, 10, 20); " +
                "INSERT INTO `Productos` (productoID, productoName, productoCB, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(NULL, 'Agua Mineral','8410732050006', 1, 0.454, 10, 0.5, 100); "+
                "INSERT INTO `Productos` (productoID, productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES " +
                "(8,'0000000000000','Ticket Cesta',4,2,0,2,1000);"+
                "INSERT INTO `Productos` (productoID, productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad)   VALUES "+
                "(9,'0000000000001','Comida',4,20,0,20,50);";


            //tabla Tipos
            query += "CREATE TABLE `productoTipo` (" +
            "`tipoID`	INTEGER PRIMARY KEY AUTOINCREMENT," +
            "`tipoName`	TEXT" +
            ");";
            //insertar Tipos
            query += "INSERT INTO `productoTipo`(tipoID,tipoName) VALUES " +
                "(1,'Bebida');" +
                "INSERT INTO `productoTipo`(tipoID,tipoName) VALUES " +
                "(2,'Comida');" +
                "INSERT INTO `productoTipo`(tipoID,tipoName) VALUES " +
                "(3,'Ropa');"+
                "INSERT INTO `productoTipo`(tipoID,tipoName) VALUES " +
                "(4,'Otros');"; 
            //tabla Sesiones
            query += "CREATE TABLE `Sesiones` (" +
                "`sesionID`	INTEGER PRIMARY KEY AUTOINCREMENT," +
                "`sesionStart`	TEXT," +
                "`sesionEnd`	TEXT," +
                "`sesionDineroInicial`	REAL," +
                "`sesionDineroFinal`	REAL," +
                "`sesionActiva`	INTEGER DEFAULT 0," +
                "`sesionDescuadre`	REAL DEFAULT 0" +
                "); ";
            //tabla pedidos
            query+="CREATE TABLE `Pedidos` (" +
	            "`pedidoID`	INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "`sesionID`	INTEGER,"+
                "`pedidoFecha`	TEXT," +
                "`pedidoTotal`	REAL," +
                "`pedidoEntregado`	REAL," +
                "`pedidoDevuelto`	REAL" +
                ");";
            //tabla pedidos detalle
            query += "CREATE TABLE `PedidosDetalle` (" +
            "`pedidoID`	INTEGER," +
            "`productoID`	INTEGER," +
            "`pdCantidad`	INTEGER" +
            ");";
            Console.WriteLine(query);

        }

        private void openConnection()
        {
            con.Open();
        }

        public void closeConnection()
        {
            query = null;
            command = null;
            con.Close();
        }

        public void executeQuery()
        {
            if (query != null) {
                openConnection();
                command = new SQLiteCommand(query, con);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SQLiteException e)
                {
                    Console.WriteLine(e.Data + "-" + query);
                }
                closeConnection();
            }
            else
            {
                Console.WriteLine("Query nula");
            }       
        }

        /*public SQLiteDataReader getResultsFromQuery() {
            openConnection();
            command.CommandText = query;
            reader = command.ExecuteReader();
            return reader;
        }*/

        public DataSet getResultsFromQuery()
        {
            openConnection();
            command = new SQLiteCommand(query, con);
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
            DataSet data = new DataSet();
            adapter.Fill(data, table);
            closeConnection();
            return data;
        }

        public SQLiteDataAdapter resultsAdapter() {
            openConnection();
            command = con.CreateCommand();
            SQLiteDataAdapter result = new SQLiteDataAdapter(query, con);
            closeConnection();
            return result;
        }


    }

    class sqlStart {
        public string startQuery;

        public sqlStart(){
            startQuery = "";

        }
    }

}




