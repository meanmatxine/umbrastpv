﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbrasTPV.Clases;

namespace umbrasTPV.Clases
{
    class ProductoTipo
    {
        public int tipoID;
        public string tipoName;

        public ProductoTipo(DataRow r) {
            tipoID = (int)r.Field<Int64>("tipoID");
            tipoName = r.Field<String>("tipoName");
        }
    }

    class TBProductType {
        private SQLite dbc;
        private DataSet data;
        DataView tabla;

        public TBProductType()
        {
            dbc = new SQLite();
            dbc.table = "productoTipo";
        }

        //Seleccionar todos
        public DataView SELECT_ALL()
        {
            dbc.query = "SELECT * FROM productoTipo";
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
            /*for (int i = 0; i < tabla.Table.Rows.Count; i++) {
                p = new Producto(tabla.Table.Rows[i]);
                //Console.WriteLine(tabla.Table.Rows[i].Field<Int64>("productoID"));
            }*/
        }
    }
}
