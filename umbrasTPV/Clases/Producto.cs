﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace umbrasTPV.Clases
{
    public class Producto
    {
        public int productoID;
        public string productoCB;
        public string productoName;
        public int productoTipo;
        public decimal productoPrecioSinIva;
        public int productoIva;
        public decimal productoPrecio;
        public int productoCantidad;

        public Producto(DataRow r) {
            productoID = (int)r.Field<Int64>("productoID");
            productoCB = r.Field<String>("productoCB");
            productoName = r.Field<String>("productoName");
            productoTipo = (int)r.Field<Int64>("productoTipo");
            productoPrecioSinIva = (decimal)(r.Field<double>("productoPrecioSinIva"));
            productoIva= (int)r.Field<Int64>("productoIva");
            productoPrecio = (decimal)(r.Field<double>("productoPrecio"));
            productoCantidad = (int)r.Field<Int64>("productoCantidad");
            //Console.WriteLine(productoID+","+productoName+","+productoPrecio+","+productoIva+","+productoCantidad);
        }

        public Producto() {

        }
    }

    class TBProducts
    {
        private SQLite dbc;
        private DataSet data;
        DataView tabla;

        public TBProducts()
        {
            dbc = new SQLite();
            dbc.table = "Productos";
        }

        //Seleccionar todos
        public DataView SELECT_ALL() {
            dbc.query = "SELECT * FROM Productos";
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
            /*for (int i = 0; i < tabla.Table.Rows.Count; i++) {
                p = new Producto(tabla.Table.Rows[i]);
                //Console.WriteLine(tabla.Table.Rows[i].Field<Int64>("productoID"));
            }*/
        }

        //todos a datagrid
        public DataTable SELECT_ALL_GRID() {
            dbc.query = "SELECT * FROM Productos";
            SQLiteDataAdapter sqlda=dbc.resultsAdapter();
            DataTable dt = new DataTable();
            sqlda.Fill(dt);
            return dt;
        }

        //Seleccion por tipo
        public DataView SELECT_TYPE(int tipo) {
            dbc.query = "SELECT * FROM Productos WHERE productoTipo=" + tipo;
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            return tabla;
        }

        //Select ONE
        public Producto SELECT_ONE(int id) {
            dbc.query = "SELECT * FROM Productos WHERE productoID=" + id;
            //Console.WriteLine(dbc.query);
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            Producto p = new Producto(tabla.Table.Rows[0]);
            return p;
        }

        //SELECT BY CB
        public Producto SELECT_BY_CB(string cb) {
            Producto p = null;
            dbc.query = "SELECT * FROM Productos WHERE productoCB='" + cb+"';";
            Console.WriteLine(dbc.query);
            data = dbc.getResultsFromQuery();
            tabla = data.Tables[dbc.table].DefaultView;
            if (tabla.Table.Rows.Count != 1) {
                p=null;
            }else
            {
                p = new Producto(tabla.Table.Rows[0]);
            }
            return p;
        }

        //ELIMINAR PRODUCTO
        public void DELETE(string id) {
            //borrar
            dbc.query = "DELETE FROM Productos WHERE productoID=" + id;
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }

        //AÑADIR PRODUCTO
        public void INSERT(Producto p) {
            //cambiar comas por puntos
            string psi = p.productoPrecioSinIva.ToString();
            psi = psi.Replace(",", ".");
            string pf = p.productoPrecio.ToString();
            pf = pf.Replace(",", ".");
            dbc.query = "INSERT INTO `Productos` VALUES (null,'"+p.productoCB+ "','" + p.productoName + "'," + p.productoTipo + ",'" + psi+ "'," + p.productoIva + ",'" + pf + "'," + p.productoCantidad + ");";
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }

        //modificar producto
        public void MODIFY(Producto p) {
            //cambiar comas por puntos
            string psi = p.productoPrecioSinIva.ToString();
            psi = psi.Replace(",", ".");
            string pf = p.productoPrecio.ToString();
            pf = pf.Replace(",", ".");
            dbc.query = "UPDATE Productos SET productoCB='" + p.productoCB + "', productoName='" + p.productoName + "', productoTipo="+p.productoTipo+", productoPrecioSinIva='"+psi+
                "', productoIva="+p.productoIva+", productoPrecio='"+pf+"', productoCantidad="+p.productoCantidad+" WHERE productoID="+p.productoID;
            dbc.executeQuery();
            //Console.WriteLine(dbc.query);
        }
    }
}
