﻿namespace umbrasTPV
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.precioTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pedidoFinalizar = new System.Windows.Forms.Button();
            this.dineroEntregado = new System.Windows.Forms.NumericUpDown();
            this.productosPanel = new System.Windows.Forms.Panel();
            this.panelTipos = new System.Windows.Forms.Panel();
            this.CBTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.App = new System.Windows.Forms.Label();
            this.newPedido = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informeDeSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.finSesionB = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cobrarDirecto = new System.Windows.Forms.Button();
            this.logoLabel = new System.Windows.Forms.Label();
            this.datosPedidoGrid = new System.Windows.Forms.DataGridView();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dineroEntregado)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidoGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // precioTotal
            // 
            this.precioTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.precioTotal.Enabled = false;
            this.precioTotal.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.precioTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.precioTotal.Location = new System.Drawing.Point(941, 438);
            this.precioTotal.Name = "precioTotal";
            this.precioTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.precioTotal.Size = new System.Drawing.Size(134, 53);
            this.precioTotal.TabIndex = 13;
            this.precioTotal.Text = "15 €";
            this.precioTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.precioTotal.UseMnemonic = false;
            // 
            // label1
            // 
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(801, 438);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(134, 53);
            this.label1.TabIndex = 4;
            this.label1.Text = "Total:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.UseMnemonic = false;
            // 
            // pedidoFinalizar
            // 
            this.pedidoFinalizar.Location = new System.Drawing.Point(693, 516);
            this.pedidoFinalizar.Name = "pedidoFinalizar";
            this.pedidoFinalizar.Size = new System.Drawing.Size(201, 74);
            this.pedidoFinalizar.TabIndex = 3;
            this.pedidoFinalizar.Text = "COBRAR";
            this.pedidoFinalizar.UseVisualStyleBackColor = true;
            this.pedidoFinalizar.Click += new System.EventHandler(this.pedidoFinalizar_Click);
            // 
            // dineroEntregado
            // 
            this.dineroEntregado.DecimalPlaces = 2;
            this.dineroEntregado.Location = new System.Drawing.Point(693, 480);
            this.dineroEntregado.Name = "dineroEntregado";
            this.dineroEntregado.Size = new System.Drawing.Size(120, 20);
            this.dineroEntregado.TabIndex = 2;
            this.dineroEntregado.Click += new System.EventHandler(this.dineroEntregado_Click);
            this.dineroEntregado.Enter += new System.EventHandler(this.dineroEntregado_Enter);
            this.dineroEntregado.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dineroEntregado_KeyDown);
            this.dineroEntregado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dineroEntregado_KeyPress);
            // 
            // productosPanel
            // 
            this.productosPanel.AutoScroll = true;
            this.productosPanel.Location = new System.Drawing.Point(12, 135);
            this.productosPanel.Name = "productosPanel";
            this.productosPanel.Size = new System.Drawing.Size(645, 478);
            this.productosPanel.TabIndex = 40;
            // 
            // panelTipos
            // 
            this.panelTipos.Location = new System.Drawing.Point(12, 71);
            this.panelTipos.Name = "panelTipos";
            this.panelTipos.Size = new System.Drawing.Size(644, 58);
            this.panelTipos.TabIndex = 7;
            // 
            // CBTextBox
            // 
            this.CBTextBox.AcceptsReturn = true;
            this.CBTextBox.Location = new System.Drawing.Point(909, 34);
            this.CBTextBox.Name = "CBTextBox";
            this.CBTextBox.Size = new System.Drawing.Size(166, 20);
            this.CBTextBox.TabIndex = 1;
            this.CBTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CBTextBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(778, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Por Codigo Barras ->";
            // 
            // App
            // 
            this.App.AutoSize = true;
            this.App.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.App.Location = new System.Drawing.Point(155, 658);
            this.App.Name = "App";
            this.App.Size = new System.Drawing.Size(313, 31);
            this.App.TabIndex = 10;
            this.App.Text = "TPV Umbras de Paradox";
            this.App.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newPedido
            // 
            this.newPedido.Location = new System.Drawing.Point(12, 27);
            this.newPedido.Name = "newPedido";
            this.newPedido.Size = new System.Drawing.Size(97, 27);
            this.newPedido.TabIndex = 12;
            this.newPedido.Text = "Pedido Nuevo";
            this.newPedido.UseVisualStyleBackColor = true;
            this.newPedido.Click += new System.EventHandler(this.newPedido_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidosToolStripMenuItem,
            this.productosToolStripMenuItem,
            this.informesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1136, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pedidosToolStripMenuItem
            // 
            this.pedidosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoPedidoToolStripMenuItem,
            this.verPedidosToolStripMenuItem});
            this.pedidosToolStripMenuItem.Name = "pedidosToolStripMenuItem";
            this.pedidosToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.pedidosToolStripMenuItem.Text = "Pedidos";
            // 
            // nuevoPedidoToolStripMenuItem
            // 
            this.nuevoPedidoToolStripMenuItem.Name = "nuevoPedidoToolStripMenuItem";
            this.nuevoPedidoToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.nuevoPedidoToolStripMenuItem.Text = "Nuevo Pedido";
            this.nuevoPedidoToolStripMenuItem.Click += new System.EventHandler(this.nuevoPedidoToolStripMenuItem_Click);
            // 
            // verPedidosToolStripMenuItem
            // 
            this.verPedidosToolStripMenuItem.Name = "verPedidosToolStripMenuItem";
            this.verPedidosToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.verPedidosToolStripMenuItem.Text = "Ver Pedidos";
            this.verPedidosToolStripMenuItem.Click += new System.EventHandler(this.verPedidosToolStripMenuItem_Click);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarProductosToolStripMenuItem});
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.productosToolStripMenuItem.Text = "Productos";
            // 
            // modificarProductosToolStripMenuItem
            // 
            this.modificarProductosToolStripMenuItem.Name = "modificarProductosToolStripMenuItem";
            this.modificarProductosToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.modificarProductosToolStripMenuItem.Text = "Modificar Productos";
            this.modificarProductosToolStripMenuItem.Click += new System.EventHandler(this.modificarProductosToolStripMenuItem_Click);
            // 
            // informesToolStripMenuItem
            // 
            this.informesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informeDeSesiónToolStripMenuItem});
            this.informesToolStripMenuItem.Name = "informesToolStripMenuItem";
            this.informesToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.informesToolStripMenuItem.Text = "Informes";
            // 
            // informeDeSesiónToolStripMenuItem
            // 
            this.informeDeSesiónToolStripMenuItem.Name = "informeDeSesiónToolStripMenuItem";
            this.informeDeSesiónToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.informeDeSesiónToolStripMenuItem.Text = "Informe de Sesión";
            this.informeDeSesiónToolStripMenuItem.Click += new System.EventHandler(this.informeDeSesiónToolStripMenuItem_Click);
            // 
            // finSesionB
            // 
            this.finSesionB.Location = new System.Drawing.Point(550, 27);
            this.finSesionB.Name = "finSesionB";
            this.finSesionB.Size = new System.Drawing.Size(107, 27);
            this.finSesionB.TabIndex = 14;
            this.finSesionB.Text = "Finalizar Sesion";
            this.finSesionB.UseVisualStyleBackColor = true;
            this.finSesionB.Click += new System.EventHandler(this.finSesionB_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(704, 453);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 15;
            this.label3.Text = "ENTREGADO";
            // 
            // cobrarDirecto
            // 
            this.cobrarDirecto.Location = new System.Drawing.Point(900, 516);
            this.cobrarDirecto.Name = "cobrarDirecto";
            this.cobrarDirecto.Size = new System.Drawing.Size(201, 74);
            this.cobrarDirecto.TabIndex = 4;
            this.cobrarDirecto.Text = "COBRAR Y NUEVO PEDIDO";
            this.cobrarDirecto.UseVisualStyleBackColor = true;
            this.cobrarDirecto.Click += new System.EventHandler(this.cobrarDirecto_Click);
            // 
            // logoLabel
            // 
            this.logoLabel.Image = global::umbrasTPV.Properties.Resources.Logotipo_small;
            this.logoLabel.Location = new System.Drawing.Point(12, 628);
            this.logoLabel.Name = "logoLabel";
            this.logoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.logoLabel.Size = new System.Drawing.Size(137, 92);
            this.logoLabel.TabIndex = 11;
            // 
            // datosPedidoGrid
            // 
            this.datosPedidoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosPedidoGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar});
            this.datosPedidoGrid.Location = new System.Drawing.Point(707, 69);
            this.datosPedidoGrid.Name = "datosPedidoGrid";
            this.datosPedidoGrid.Size = new System.Drawing.Size(394, 348);
            this.datosPedidoGrid.TabIndex = 17;
            this.datosPedidoGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datosPedidoGrid_CellContentClick);
            // 
            // Quitar
            // 
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            this.Quitar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Quitar.Text = "X";
            this.Quitar.ToolTipText = "Quitar";
            this.Quitar.UseColumnTextForButtonValue = true;
            this.Quitar.Width = 40;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 729);
            this.Controls.Add(this.datosPedidoGrid);
            this.Controls.Add(this.cobrarDirecto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.finSesionB);
            this.Controls.Add(this.newPedido);
            this.Controls.Add(this.logoLabel);
            this.Controls.Add(this.App);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CBTextBox);
            this.Controls.Add(this.panelTipos);
            this.Controls.Add(this.dineroEntregado);
            this.Controls.Add(this.pedidoFinalizar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.precioTotal);
            this.Controls.Add(this.productosPanel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UmbrasTPV";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dineroEntregado)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidoGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label precioTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button pedidoFinalizar;
        private System.Windows.Forms.NumericUpDown dineroEntregado;
        private System.Windows.Forms.Panel productosPanel;
        private System.Windows.Forms.Panel panelTipos;
        private System.Windows.Forms.TextBox CBTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label App;
        private System.Windows.Forms.Label logoLabel;
        private System.Windows.Forms.Button newPedido;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarProductosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informeDeSesiónToolStripMenuItem;
        private System.Windows.Forms.Button finSesionB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cobrarDirecto;
        private System.Windows.Forms.ToolStripMenuItem verPedidosToolStripMenuItem;
        private System.Windows.Forms.DataGridView datosPedidoGrid;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
    }
}

