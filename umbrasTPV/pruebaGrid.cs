﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace umbrasTPV
{
    public partial class pruebaGrid : Form
    {
        List<pruebaP> pruebaList;
        BindingList<pruebaP> binding;
        public pruebaGrid()
        {
            InitializeComponent();
        }

        private void pruebaGrid_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            //dataGridView1.AllowUserToAddRows = true;
            pruebaList = new List<pruebaP>();
            //binding = new BindingList<pruebaP>();
            for (int i = 0; i <= 5; i++) {
                pruebaP p = new pruebaP("prueba" + i, 5 * i, 10 * i);
                pruebaList.Add(p);
                //binding.Add(p);
            }
            dataGridView1.DataSource = pruebaList;
            binding = new BindingList<pruebaP>(pruebaList);
            /*var source = new BindingSource();
            source.DataSource = binding;*/
            //dataGridView1.DataSource = binding;
            foreach (pruebaP pp in binding) {
                Console.WriteLine(pp.nombre + "-" + pp.cantidad + "-" + pp.precio);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("LLenar Grid");
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = pruebaList;
        }
    }

    public class pruebaP {
        public string nombre;
        public int cantidad;
        public int precio;

        public pruebaP(string n, int c, int p) {
            nombre = n;
            cantidad = c;
            precio = p;
        }
    }
}
