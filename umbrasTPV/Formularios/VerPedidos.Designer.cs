﻿namespace umbrasTPV.Formularios
{
    partial class VerPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.datosPedidos = new System.Windows.Forms.DataGridView();
            this.Ver = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.datosDetalle = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datosDetalle)).BeginInit();
            this.SuspendLayout();
            // 
            // datosPedidos
            // 
            this.datosPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosPedidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ver});
            this.datosPedidos.Location = new System.Drawing.Point(29, 33);
            this.datosPedidos.Name = "datosPedidos";
            this.datosPedidos.Size = new System.Drawing.Size(811, 150);
            this.datosPedidos.TabIndex = 0;
            this.datosPedidos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datosPedidos_CellContentClick);
            // 
            // Ver
            // 
            this.Ver.HeaderText = "Ver";
            this.Ver.Name = "Ver";
            this.Ver.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Ver.Text = "Ver";
            this.Ver.UseColumnTextForButtonValue = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "PEDIDOS";
            // 
            // datosDetalle
            // 
            this.datosDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosDetalle.Location = new System.Drawing.Point(29, 236);
            this.datosDetalle.Name = "datosDetalle";
            this.datosDetalle.Size = new System.Drawing.Size(810, 142);
            this.datosDetalle.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "DETALLES PEDIDO";
            // 
            // VerPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 398);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.datosDetalle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.datosPedidos);
            this.Name = "VerPedidos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VerPedidos";
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datosDetalle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView datosPedidos;
        private System.Windows.Forms.DataGridViewButtonColumn Ver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView datosDetalle;
        private System.Windows.Forms.Label label2;
    }
}