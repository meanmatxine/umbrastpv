﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV.Clases;

namespace umbrasTPV
{
    public partial class Productos : Form
    {
        TBProducts tbProducts;
        int modProdID=100;
        DataTable dt;

        public Productos()
        {
            InitializeComponent();
            tbProducts = new TBProducts();
            dt = tbProducts.SELECT_ALL_GRID();
            datosProductos.DataSource = dt;
        }

        private void refButton_Click(object sender, EventArgs e)
        {
            refrescarGrid();
            
        }

        private void datosProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //ELIMINAR
                DialogResult dr = MessageBox.Show("Esta Seguro Que Desea Eliminar el Producto " + datosProductos.Rows[e.RowIndex].Cells[3].Value.ToString() + ", ESTA ACCION BORRARA EL PRODUCTO DE LA LISTA", "Eliminar Producto", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    tbProducts.DELETE(datosProductos.Rows[e.RowIndex].Cells[2].Value.ToString());
                    refrescarGrid();
                }
            }
            else if (e.ColumnIndex == 1) {
                //MODIFICAR
                //poner los datos en el formulario
                textCB.Text= datosProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                textName.Text= datosProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
                comboTipo.SelectedItem = setProductType(datosProductos.Rows[e.RowIndex].Cells[5].Value.ToString());
                numericSinIVA.Value= decimal.Parse(datosProductos.Rows[e.RowIndex].Cells[6].Value.ToString());
                numericIVA.Value= int.Parse(datosProductos.Rows[e.RowIndex].Cells[7].Value.ToString()); 
                numericPrecioFinal.Value= decimal.Parse(datosProductos.Rows[e.RowIndex].Cells[8].Value.ToString());
                numericCantidad.Value= int.Parse(datosProductos.Rows[e.RowIndex].Cells[9].Value.ToString());
                //id
                modProdID = int.Parse(datosProductos.Rows[e.RowIndex].Cells[2].Value.ToString());
                //botones
                btnModify.Visible = true;
                buttonAdd.Visible = false;
            }           
        }

        private void volverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //añadir producto
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (comprobarValores())
            {
                Producto p = new Producto();
                p.productoCB = textCB.Text;
                p.productoName = textName.Text;
                p.productoTipo = getProductType(comboTipo.SelectedItem.ToString());
                p.productoPrecioSinIva = numericSinIVA.Value;
                p.productoIva = (int)numericIVA.Value;
                p.productoPrecio = numericPrecioFinal.Value;
                p.productoCantidad = (int)numericCantidad.Value;
                tbProducts.INSERT(p);
            }
            else {
                MessageBox.Show("Alguno de los valores no es valido, revisalos", "Valores", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //limpiar formulario
        private void buttonLimpiar_Click(object sender, EventArgs e)
        {
            textCB.Text = "";
            textName.Text = "";
            comboTipo.SelectedItem = "Comida";
            numericSinIVA.Value = 0;
            numericIVA.Value = 0;
            numericPrecioFinal.Value = 0;
            numericCantidad.Value = 0;
            //botones
            btnModify.Visible = false;
            buttonAdd.Visible = true;
            //Id
            modProdID = 100;
        }

        //comprobar valores
        bool comprobarValores() {
            bool b = true;
            int pt = getProductType(comboTipo.SelectedItem.ToString());
            if (textCB.Text == "" || textName.Text == "" || numericSinIVA.Value == 0 || numericIVA.Value == 0 || numericPrecioFinal.Value == 0 || pt==0 || numericPrecioFinal.Value<numericSinIVA.Value) {
                b = false;
            }
            return b;
        }

        //coger tipo de producto
        int getProductType(string t) {
            int pt = 0;
            switch (t) {
                case "Comida":
                    pt = 2;
                    break;
                case "Bebida":
                    pt = 1;
                    break;
                case "Ropa":
                    pt = 3;
                    break;
                case "Otros":
                    pt = 4;
                    break;
            }
            return pt;
        }
        //seleccionar tipo de producto
        string setProductType(string s) {
            string pt = "";
            switch (s) {
                case "1":
                    pt = "Bebida";
                    break;
                case "2":
                    pt = "Comida";
                    break;
                case "3":
                    pt = "Ropa";
                    break;
                case "4":
                    pt = "Otros";
                    break;
            }
            return pt;
        }

        //modificar producto
        private void btnModify_Click(object sender, EventArgs e)
        {
            if (comprobarValores())
            {
                Producto p = new Producto();
                p.productoID = modProdID;
                p.productoCB = textCB.Text;
                p.productoName = textName.Text;
                p.productoTipo = getProductType(comboTipo.SelectedItem.ToString());
                p.productoPrecioSinIva = numericSinIVA.Value;
                p.productoIva = (int)numericIVA.Value;
                p.productoPrecio = numericPrecioFinal.Value;
                p.productoCantidad = (int)numericCantidad.Value;
                tbProducts.MODIFY(p);
                //refrescar grid
                refrescarGrid();
            }
            else
            {
                MessageBox.Show("Alguno de los valores no es valido, revisalos", "Valores", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //refrescar Grid
        private void refrescarGrid() {
            dt = tbProducts.SELECT_ALL_GRID();
            datosProductos.DataSource = dt;
        }

        //Puntos Por Comas
        private void numericSinIVA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        private void numericPrecioFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }
    }
}
