﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV.Clases;
using System.Drawing.Printing;
using System.Globalization;

namespace umbrasTPV
{
    public partial class pedidoFinal : Form
    {
        TBPedidos tbpedidos;
        TBPedidosDetalle tbDetalles;
        TBProducts tbproducts;
        TBSesion tbsesion;
        Pedido pedido;
        List<Producto> pDetalle;
        //pedido cancelado
        public bool pedidoCancelado;
        int alturaLinea = 20;
        public pedidoFinal(Pedido p, List<Producto> pDet)
        {
            InitializeComponent();
            pedidoCancelado = false;
            pedido = p;
            Console.WriteLine(pedido.pedidoTotal+"--"+p.pedidoTotal);
            pDetalle = new List<Producto>();
            pDetalle = pDet;
            //tabla productos
            tbproducts = new TBProducts();
            tbpedidos = new TBPedidos();
            tbDetalles = new TBPedidosDetalle();
            tbsesion = new TBSesion();
            listPedido.RowCount = 1;
            listPedido.RowStyles.Clear();
            listPedido.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            listPedido.Controls.Add(new Label() { Text = "Producto" }, 0, 0);
            listPedido.Controls.Add(new Label() { Text = "Cant" }, 1, 0);
            listPedido.Controls.Add(new Label() { Text = "Precio ud" }, 2, 0);
            listPedido.Controls.Add(new Label() { Text = "Total" }, 3, 0);
            foreach (Producto pd in pDet) {
                if (pd.productoCantidad > 0) {
                    int linea = listPedido.RowCount;
                    listPedido.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
                    listPedido.Controls.Add(new Label() { Text = pd.productoName }, 0, linea);
                    listPedido.Controls.Add(new Label() { Text = pd.productoCantidad.ToString() }, 1, linea);
                    listPedido.Controls.Add(new Label() { Text = pd.productoPrecio.ToString() }, 2, linea);
                    listPedido.Controls.Add(new Label() { Text = (pd.productoPrecio * pd.productoCantidad).ToString() }, 3, linea);
                    listPedido.RowCount++; 
                }
            }
            //Total
            total.Text = "Total: " + p.pedidoTotal.ToString();
            //entregado
            entregado.Text = "Entregado: " + p.pedidoEntregado.ToString();
            //a devolver
            devolver.Text = "A Devolver: " + p.pedidoDevuelto.ToString();

        }

        //cancelar pedido
        private void cancelButton_Click(object sender, EventArgs e)
        {
            pedidoCancelado = true;
            this.Close();
        }

        //validar agregar a la base de datos y volver al formulario
        private void ValidarButton_Click(object sender, EventArgs e)
        {
            //insertamos pedido en la dbc
            insertarPedido();
            //cerrarmos y volvemos
            this.Close();
           
        }

        //imprimir ticket
        private void printbutton_Click(object sender, EventArgs e)
        {
            //insertamos pedido en la dbc
            insertarPedido();
            //impresion
            PrintDocument pd = new PrintDocument();
            //alturaPapel = calcularAltura();
            PaperSize ps = new PaperSize("A4", 850, 1100);
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.PrintController = new StandardPrintController();
            pd.DefaultPageSettings.Margins.Left = 0;
            pd.DefaultPageSettings.Margins.Right = 0;
            pd.DefaultPageSettings.Margins.Top = 0;
            pd.DefaultPageSettings.Margins.Bottom = 0;
            pd.DefaultPageSettings.PaperSize = ps;
            //pruebas
            PrintDialog pDialog = new PrintDialog();
            pDialog.Document = pd;
            DialogResult res = pDialog.ShowDialog();
            if (res == DialogResult.OK) {
                pd.Print();
                this.Close();
            }
        }


        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            //g.DrawRectangle(Pens.Black, 5, 5, 240, alturaPapel-5);
            Console.WriteLine("path: "+Application.StartupPath);
            string title = Application.StartupPath + "\\Logotipo.png";
            g.DrawImage(Image.FromFile(title), 20, 10);
            Font fBody = new Font("LucidaConsole", 11, FontStyle.Bold);
            Font fTitle = new Font("LucidaConsole", 14, FontStyle.Bold);
            SolidBrush sb = new SolidBrush(Color.Black);
            g.DrawString("Alter Paradox", fTitle, sb, 120, 50);
            g.DrawString("X Umbras de Paradox - 2017", fBody, sb, 120, 70);
            //fecha
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, 100);
            DateTime localDate = DateTime.Now;
            var culture = new CultureInfo("es-ES");
            string fecha= localDate.ToString(culture);
            
            g.DrawString("Fecha: "+fecha, fBody, sb, 10, 130);
            //cogerpedido
            Pedido pe = tbpedidos.GetLast();
            g.DrawString("Factura Pedido " + pe.pedidoID, fBody, sb, 10, 150);
            //datos pedido
            int colProducto, colCant, colPrecio, colIva, colFinal, colTotal;
            colProducto = 10;
            colPrecio = 200;
            colIva = 300;
            colFinal = 450;
            colCant = 550;
            colTotal = 600;
            //tabla head
            g.DrawString("Producto",fBody,sb,colProducto, 170);
            g.DrawString("Precio", fBody, sb, colPrecio, 170);
            g.DrawString("IVA", fBody, sb, colIva, 170);
            g.DrawString("Precio Final", fBody, sb, colFinal, 170);
            g.DrawString("Cant", fBody, sb, colCant, 170);
            g.DrawString("Total", fBody, sb, colTotal, 170);
            //detalles pedido
            int lineaInicial = 190;
            int linea =0;
            foreach  (Producto pde in pDetalle)
            {
                if (pde.productoCantidad > 0) {
                    g.DrawString(pde.productoName, fBody, sb, colProducto, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoCantidad.ToString(), fBody, sb, colCant, lineaInicial + (linea * alturaLinea));
                    //decimal precioSIva = (calcularSinIva(pde));
                    g.DrawString(pde.productoPrecioSinIva + " €", fBody, sb, colPrecio, lineaInicial + (linea * alturaLinea));
                    decimal iva = calcularIva(pde);
                    g.DrawString("IVA (" + pde.productoIva + "): " + iva + " €", fBody, sb, colIva, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoPrecio+ " €", fBody, sb, colFinal, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoCantidad * pde.productoPrecio + " €", fBody, sb, colTotal, lineaInicial + (linea * alturaLinea));
                    linea++;
                }  
            }
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("TOTAL: " + pedido.pedidoTotal+" €", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("entregado: " + pedido.pedidoEntregado+ "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("Devuelto: " + pedido.pedidoTotal+ "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            g.Dispose();
        }

        int calcularAltura() {
            return 250+(pDetalle.Count*alturaLinea);
        }

        //calcular precio sin iva
        decimal calcularSinIva(Producto p) {
            Decimal d = (decimal)p.productoPrecio / (1 + (p.productoIva / 100));
            d = Math.Round(d, 2);
            return d;
        }

        //calcular IVA
        decimal calcularIva(Producto p) {
            decimal d=(p.productoPrecioSinIva * p.productoIva) / 100;
            d = Math.Round(d, 2);
            return d;
        }

        void insertarPedido() {
            //insertamos el pedido
            tbpedidos.INSERT(pedido);
            //cogemos el ultimo pedido
            Pedido ultimo = tbpedidos.GetLast();
            //agregamos el id al detalle e insertamos en DBC
            foreach (Producto pd in pDetalle)
            {
                if (pd.productoCantidad > 0)
                {
                    PedidoDetalle pdet = new PedidoDetalle();
                    pdet.pedidoID = ultimo.pedidoID;
                    pdet.productoID = pd.productoID;
                    pdet.pdCantidad = pd.productoCantidad;
                    tbDetalles.Insert(pdet);
                }
            }
            //miramos la sesion que es y sumamos para el final
            Sesion sesionActiva = tbsesion.SELECT_ACTIVE();
            decimal sumar = sesionActiva.sesionDineroFinal + pedido.pedidoTotal;
            tbsesion.UPDATETOTAL(sumar, sesionActiva.sesionID);
        }
    }
}
