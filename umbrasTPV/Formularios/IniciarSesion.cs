﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV.Clases;

namespace umbrasTPV
{
    public partial class IniciarSesion : Form
    {
        TBSesion tbSesion;
        Sesion sesionView;

        public IniciarSesion()
        {
            InitializeComponent();
        }

        private void IniciarSesion_Load(object sender, EventArgs e)
        {
            tbSesion = new TBSesion();
            sesionView = tbSesion.SELECT_ACTIVE();
            //si hay otra sesion abierta mostrar error
            if (sesionView != null) {
                MessageBox.Show("Oups, esto no deberia haber ocurrido, reinicie la aplicacin y si sigue viendo el mensaje localize a fermin, si no esta siga trabajando y desee que luego cuadren las cuentas, porque si no el dios Zeus bajara y te metera un rayo por el culo. Y a Fermín dos.", "Error inexperado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonIniciar_Click(object sender, EventArgs e)
        {
            if (dineroInicial.Value < 0)
            {
                MessageBox.Show("Valor no Válido", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                //iniciar la sesion
                Sesion s = new Sesion();
                DateTime localDate = DateTime.Now;
                var culture = new CultureInfo("es-ES");
                s.sesionStart = localDate.ToString(culture);
                s.sesionEnd = "null";
                s.sesionDineroInicial = dineroInicial.Value;
                s.sesionDineroFinal = 0;
                s.sesionDescuadre = 0;
                s.sesionActiva = 1;
                tbSesion.INSERT(s);
                this.Close();

            }
        }

        private void dineroInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }
    }
}
