﻿namespace umbrasTPV
{
    partial class Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.datosProductos = new System.Windows.Forms.DataGridView();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.productoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.refButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.volverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.textCB = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboTipo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericSinIVA = new System.Windows.Forms.NumericUpDown();
            this.numericPrecioFinal = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericCantidad = new System.Windows.Forms.NumericUpDown();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonLimpiar = new System.Windows.Forms.Button();
            this.numericIVA = new System.Windows.Forms.NumericUpDown();
            this.btnModify = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.datosProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSinIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrecioFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIVA)).BeginInit();
            this.SuspendLayout();
            // 
            // datosProductos
            // 
            this.datosProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Eliminar,
            this.Modificar});
            this.datosProductos.Location = new System.Drawing.Point(25, 29);
            this.datosProductos.Name = "datosProductos";
            this.datosProductos.Size = new System.Drawing.Size(1086, 239);
            this.datosProductos.TabIndex = 0;
            this.datosProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datosProductos_CellContentClick);
            // 
            // Eliminar
            // 
            this.Eliminar.Frozen = true;
            this.Eliminar.HeaderText = "Eliminar";
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.ReadOnly = true;
            this.Eliminar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Eliminar.Text = "Eliminar";
            this.Eliminar.UseColumnTextForButtonValue = true;
            // 
            // Modificar
            // 
            this.Modificar.Frozen = true;
            this.Modificar.HeaderText = "Modificar";
            this.Modificar.Name = "Modificar";
            this.Modificar.ReadOnly = true;
            this.Modificar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Modificar.Text = "Modificar";
            this.Modificar.UseColumnTextForButtonValue = true;
            // 
            // productoBindingSource
            // 
            this.productoBindingSource.DataSource = typeof(umbrasTPV.Clases.Producto);
            // 
            // refButton
            // 
            this.refButton.ForeColor = System.Drawing.Color.DarkCyan;
            this.refButton.Location = new System.Drawing.Point(25, 274);
            this.refButton.Name = "refButton";
            this.refButton.Size = new System.Drawing.Size(75, 23);
            this.refButton.TabIndex = 1;
            this.refButton.Text = "Refrescar";
            this.refButton.UseVisualStyleBackColor = true;
            this.refButton.Click += new System.EventHandler(this.refButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.volverToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1147, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // volverToolStripMenuItem
            // 
            this.volverToolStripMenuItem.Name = "volverToolStripMenuItem";
            this.volverToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.volverToolStripMenuItem.Text = "Volver";
            this.volverToolStripMenuItem.Click += new System.EventHandler(this.volverToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(698, 326);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Codigo Barras";
            // 
            // textCB
            // 
            this.textCB.Location = new System.Drawing.Point(788, 320);
            this.textCB.Name = "textCB";
            this.textCB.Size = new System.Drawing.Size(200, 20);
            this.textCB.TabIndex = 4;
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(788, 357);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(200, 20);
            this.textName.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(698, 363);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre Producto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(698, 402);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tipo Producto";
            // 
            // comboTipo
            // 
            this.comboTipo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboTipo.FormattingEnabled = true;
            this.comboTipo.Items.AddRange(new object[] {
            "Comida",
            "Bebida",
            "Ropa",
            "Otros"});
            this.comboTipo.Location = new System.Drawing.Point(788, 394);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.Size = new System.Drawing.Size(200, 21);
            this.comboTipo.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(698, 437);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Precio Sin IVA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(698, 474);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "IVA Aplicable";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(698, 509);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Precio Final";
            // 
            // numericSinIVA
            // 
            this.numericSinIVA.DecimalPlaces = 2;
            this.numericSinIVA.InterceptArrowKeys = false;
            this.numericSinIVA.Location = new System.Drawing.Point(788, 432);
            this.numericSinIVA.Name = "numericSinIVA";
            this.numericSinIVA.Size = new System.Drawing.Size(200, 20);
            this.numericSinIVA.TabIndex = 15;
            this.numericSinIVA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericSinIVA_KeyPress);
            // 
            // numericPrecioFinal
            // 
            this.numericPrecioFinal.DecimalPlaces = 2;
            this.numericPrecioFinal.Location = new System.Drawing.Point(788, 507);
            this.numericPrecioFinal.Name = "numericPrecioFinal";
            this.numericPrecioFinal.Size = new System.Drawing.Size(200, 20);
            this.numericPrecioFinal.TabIndex = 16;
            this.numericPrecioFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericPrecioFinal_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(698, 545);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Cantidad";
            // 
            // numericCantidad
            // 
            this.numericCantidad.Location = new System.Drawing.Point(788, 543);
            this.numericCantidad.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericCantidad.Name = "numericCantidad";
            this.numericCantidad.Size = new System.Drawing.Size(200, 20);
            this.numericCantidad.TabIndex = 18;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(788, 584);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(134, 29);
            this.buttonAdd.TabIndex = 19;
            this.buttonAdd.Text = "Nuevo Producto";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonLimpiar
            // 
            this.buttonLimpiar.Location = new System.Drawing.Point(701, 584);
            this.buttonLimpiar.Name = "buttonLimpiar";
            this.buttonLimpiar.Size = new System.Drawing.Size(87, 29);
            this.buttonLimpiar.TabIndex = 20;
            this.buttonLimpiar.Text = "Limpiar";
            this.buttonLimpiar.UseVisualStyleBackColor = true;
            this.buttonLimpiar.Click += new System.EventHandler(this.buttonLimpiar_Click);
            // 
            // numericIVA
            // 
            this.numericIVA.Location = new System.Drawing.Point(788, 472);
            this.numericIVA.Name = "numericIVA";
            this.numericIVA.Size = new System.Drawing.Size(200, 20);
            this.numericIVA.TabIndex = 21;
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(928, 584);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(134, 29);
            this.btnModify.TabIndex = 22;
            this.btnModify.Text = "Modificar Producto";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Visible = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 638);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.numericIVA);
            this.Controls.Add(this.buttonLimpiar);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.numericCantidad);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericPrecioFinal);
            this.Controls.Add(this.numericSinIVA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboTipo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textCB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.refButton);
            this.Controls.Add(this.datosProductos);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Productos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Productos";
            ((System.ComponentModel.ISupportInitialize)(this.datosProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSinIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPrecioFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIVA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView datosProductos;
        private System.Windows.Forms.BindingSource productoBindingSource;
        private System.Windows.Forms.Button refButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem volverToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textCB;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboTipo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericSinIVA;
        private System.Windows.Forms.NumericUpDown numericPrecioFinal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericCantidad;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonLimpiar;
        private System.Windows.Forms.NumericUpDown numericIVA;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;
        private System.Windows.Forms.DataGridViewButtonColumn Modificar;
        private System.Windows.Forms.Button btnModify;
    }
}