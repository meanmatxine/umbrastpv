﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV.Clases;

namespace umbrasTPV.Formularios
{
    public partial class CerrarSesion : Form
    {
        //sesion
        Sesion sesionActiva;
        TBPedidos pedidosSesionTable;
        List<Pedido> pedidosSesionList;
        TBSesion sesionTable;
        DataView pedidosSesionView;
        decimal totalSesion;
        decimal totalReal;
        decimal descuadre;
        decimal cajaFinal;
        public CerrarSesion()
        {
            InitializeComponent();
            totalReal = descuadre=cajaFinal=0;
            //iniciamos tablas
            pedidosSesionTable = new TBPedidos();
            sesionTable = new TBSesion();
            //cogemos la sesion activa
            sesionActiva = sesionTable.SELECT_ACTIVE();
            //pedidos de la sesion
            pedidosSesionView = pedidosSesionTable.pedidosDeSesion(sesionActiva.sesionID);
            //ajustar dinero inicial y final
            pedidosSesionList = new List<Pedido>();
            totalSesion= 0;
            for (int i = 0; i < pedidosSesionView.Count; i++) {
                Pedido p = new Pedido(pedidosSesionView.Table.Rows[i]);
                totalSesion += p.pedidoTotal;
            }
            //ajustar texto, segun caja y descuadre
            textCaja.Text = (totalSesion + sesionActiva.sesionDineroInicial)+" €";
            descuadre = totalReal - (totalSesion + sesionActiva.sesionDineroInicial);
            textDescuadre.Text = descuadre + "€";

        }

        // puntos x comas en numSacar
        private void numSacar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        // puntos x comas en numMeter
        private void numMeter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        void calcularTotal() {
            //calcular labels
            decimal m001, m002, m005, m01, m02, m05, m1, m2, b5, b10, b20, b50=0;
            //001
            m001 = monedas001.Value * (decimal)0.01;
            total001.Text = m001 + "€";
            //002
            m002 = monedas002.Value * (decimal)0.02;
            total002.Text = m002 + "€";
            //005
            m005 = monedas005.Value * (decimal)0.05;
            total005.Text = m005 + "€";
            //01
            m01 = monedas01.Value*(decimal)0.1;
            total01.Text = m01 + "€";
            //02
            m02 = monedas02.Value * (decimal)0.2;
            total02.Text = m02 + "€";
            //05
            m05 = monedas05.Value * (decimal)0.5;
            total05.Text = m05 + "€";
            //1
            m1 = monedas1.Value * (decimal)1;
            total1.Text = m1 + "€";
            //2
            m2 = monedas2.Value * (decimal)2;
            total2.Text = m2 + "€";
            //5
            b5 = billete5.Value * (decimal)5;
            total5.Text = b5 + "€";
            //10
            b10 = billete10.Value * (decimal)10;
            total10.Text = b10 + "€";
            //20
            b20 = billete20.Value * (decimal)20;
            total20.Text = b20 + "€";
            //50
            b50 = billete50.Value * (decimal)50;
            total50.Text = b50 + "€";
            //calcularTotal
            totalReal = m001 + m002 + m005 + m01 + m02 + m05 + m1 + m2 + b5 + b10 + b20 + b50;
            //ajustar total
            textTotal.Text = totalReal + "€";
            //ajustar descuadre
            descuadre = totalReal - (totalSesion + sesionActiva.sesionDineroInicial);
            textDescuadre.Text = descuadre + "€";
            //Ajustar total Caja Final
            cajaFinal = (totalReal + numMeter.Value) - numSacar.Value;
            textActual.Text = cajaFinal + "€";
        }

        private void monedas001_ValueChanged(object sender, EventArgs e)
        {
            calcularTotal();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            calcularTotal();
            //cerramos sesion actual
            DateTime localDate = DateTime.Now;
            var culture = new CultureInfo("es-ES");
            sesionActiva.sesionEnd= localDate.ToString(culture);
            sesionActiva.sesionDineroFinal = cajaFinal;
            sesionActiva.sesionDescuadre = descuadre;
            sesionActiva.sesionActiva = 0;
            //insertamos datos
            sesionTable.UPDATE(sesionActiva);
            //generamos nueva sesion
            Sesion s = new Sesion();
            s.sesionStart= localDate.ToString(culture);
            s.sesionDineroInicial = cajaFinal;
            s.sesionActiva = 1;
            s.sesionEnd = "null";
            s.sesionDineroFinal = 0;
            s.sesionDescuadre = 0;
            sesionTable.INSERT(s);
            //cerramos formulario
            this.Close();
        }

        private void monedas001_Click(object sender, EventArgs e)
        {

            NumericUpDown nd = sender as NumericUpDown;
            //nd.Select(0, 4);
            
        }

    }
}
