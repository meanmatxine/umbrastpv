﻿namespace umbrasTPV
{
    partial class IniciarSesion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dineroInicial = new System.Windows.Forms.NumericUpDown();
            this.buttonIniciar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dineroInicial)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dinero Inicial: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.UseWaitCursor = true;
            // 
            // dineroInicial
            // 
            this.dineroInicial.DecimalPlaces = 2;
            this.dineroInicial.Location = new System.Drawing.Point(213, 30);
            this.dineroInicial.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.dineroInicial.Name = "dineroInicial";
            this.dineroInicial.Size = new System.Drawing.Size(71, 20);
            this.dineroInicial.TabIndex = 1;
            this.dineroInicial.UseWaitCursor = true;
            this.dineroInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dineroInicial_KeyPress);
            // 
            // buttonIniciar
            // 
            this.buttonIniciar.Location = new System.Drawing.Point(138, 77);
            this.buttonIniciar.Name = "buttonIniciar";
            this.buttonIniciar.Size = new System.Drawing.Size(114, 33);
            this.buttonIniciar.TabIndex = 2;
            this.buttonIniciar.Text = "Iniciar Sesion";
            this.buttonIniciar.UseVisualStyleBackColor = true;
            this.buttonIniciar.UseWaitCursor = true;
            this.buttonIniciar.Click += new System.EventHandler(this.buttonIniciar_Click);
            // 
            // IniciarSesion
            // 
            this.AcceptButton = this.buttonIniciar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 180);
            this.ControlBox = false;
            this.Controls.Add(this.buttonIniciar);
            this.Controls.Add(this.dineroInicial);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "IniciarSesion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IniciarSesion";
            this.UseWaitCursor = true;
            this.Load += new System.EventHandler(this.IniciarSesion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dineroInicial)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown dineroInicial;
        private System.Windows.Forms.Button buttonIniciar;
    }
}