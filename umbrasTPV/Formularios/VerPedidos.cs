﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using umbrasTPV.Clases;

namespace umbrasTPV.Formularios
{
    public partial class VerPedidos : Form
    {
        TBPedidos tbPedidos;
        TBProducts tbProductos;
        TBPedidosDetalle tbDetalles;
        DataTable dtPed;
        DataTable dtDet;
        List<PedidoDetalle> listDetalles;

        public VerPedidos()
        {
            InitializeComponent();
            tbPedidos = new TBPedidos();
            tbDetalles = new TBPedidosDetalle();
            tbProductos = new TBProducts();
            dtPed = tbPedidos.SELECT_ALL_GRID_REVERSE();
            datosPedidos.DataSource = dtPed;
            listDetalles = new List<PedidoDetalle>();
        }

        private void datosPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //VER
            if (e.ColumnIndex == 0)
            {
                //limpiar lista
                listDetalles.Clear();
                //Seleccionar pedido
                int pID = int.Parse(datosPedidos.Rows[e.RowIndex].Cells[1].Value.ToString());
                //cofer detalles de ese pedido
                //crear data table
                dtDet = new DataTable();
                dtDet.Columns.Add(new DataColumn("Producto"));
                dtDet.Columns.Add(new DataColumn("Cantidad"));
                dtDet.Columns.Add(new DataColumn("Precio"));
                dtDet.Columns.Add(new DataColumn("Total"));
                //detalles pedido
                listDetalles = tbDetalles.SELECT_ONE_GRID(pID);
                foreach (PedidoDetalle pd in listDetalles) {
                    //nombre
                    Producto p = tbProductos.SELECT_ONE(pd.productoID);
                    decimal total = p.productoPrecio * pd.pdCantidad;
                    dtDet.Rows.Add(new Object[] { p.productoName,pd.pdCantidad,p.productoPrecio,total});
                }
                datosDetalle.DataSource = dtDet;
                
            }
        }
    }
}
